# MVI-SP
* **Description of task**: [Kaggle task](https://www.kaggle.com/Cornell-University/arxiv/tasks?taskId=1757) or [Medium](https://lenoctom.medium.com/multi-label-classification-of-arxiv-dataset-using-sbert-and-doc2vec-4875277327b9) or in report
* Source code is possible to run using: [Google Colab](https://colab.research.google.com/drive/10UCw7CxzuSF-Ml8v29mB9Hay7OAYmrls?usp=sharing) or download notebook file from repo.
---------
### Presentation
As a presentation of my work I wrote [Medium](https://lenoctom.medium.com/multi-label-classification-of-arxiv-dataset-using-sbert-and-doc2vec-4875277327b9) article.

